package reader;

import model.PrecedenceConstraint;
import model.Problem;
import model.Task;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.*;

/**
 * Handler for parsing task xml in format:
 * <pre>{@code
 *     <problem pe="3">
 *      <tasks>
 *          <task>
 *              <title>Red</title>
 *              <computationCost>3</computationCost>
 *          </task>
 *          <task>
 *              <title>Green</title>
 *              <computationCost>5</computationCost>
 *          </task>
 *          <task>
 *              <title>Blue</title>
 *              <computationCost>7</computationCost>
 *          </task>
 *      </tasks>
 * <p/>
 *      <precedences>
 *          <precedence>
 *              <parent>Red</parent>
 *              <child>Blue</child>
 *              <cost>1</cost>
 *          </precedence>
 *      </precedences>
 * </problem>
 * }</pre>
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class TaskSaxHandler extends DefaultHandler {
    private final static String PROBLEM = "problem";
    private final static String TASKS = "tasks";
    private final static String TASK = "task";
    private final static String TITLE = "title";
    private final static String COMPUTATION_COST = "computationCost";
    private final static String PRECEDENCES = "precedences";
    private final static String PRECEDENCE = "precedence";
    private final static String PARENT = "parent";
    private final static String CHILD = "child";
    private final static String TRANSPORTATION_COST = "transportationCost";
    public static final String PE = "pe";

    private Problem problem;
    private StringBuffer text;
    private int processorsCount;
    private List<Task> tasks;
    private Map<String, Task> tasksMap;
    private String title;
    private float computationCost = -1;
    private List<PrecedenceConstraint> precedences;
    private String parent;
    private String child;
    private float transportationCost = -1;

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        text.append(ch, start, length);
        super.characters(ch, start, length);
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
    }

    @Override
    public void startElement(String uri, String name, String localName, Attributes attributes) throws SAXException {
        if (localName.equalsIgnoreCase(PROBLEM)) {
            processorsCount = Integer.parseInt(attributes.getValue(PE));
            problem = null;
            tasks = null;
            tasksMap = null;
            title = null;
            computationCost = -1;
            precedences = null;
            parent = null;
            child = null;
            transportationCost = -1;
        } else if (localName.equalsIgnoreCase(TASKS)) {
            if (tasks != null) {
                throw new SAXException("malformed xml: duplicate tasks block?");
            }
            tasks = new ArrayList<Task>();
            tasksMap = new HashMap<String, Task>();
        } else if (localName.equalsIgnoreCase(PRECEDENCES)) {
            if (precedences != null) {
                throw new SAXException("malformed xml: duplicate precedences block?");
            }
            precedences = new ArrayList<PrecedenceConstraint>();
        } else if (localName.equalsIgnoreCase(TASK)) {
            computationCost = -1;
            title = null;
        }
        text = new StringBuffer();
        super.startElement(uri, localName, name, attributes);
    }

    @Override
    public void endElement(String uri, String qName, String localName) throws SAXException {
        if (localName.equalsIgnoreCase(TASK)) {
            if (computationCost == -1 || title == null) {
                throw new SAXException("malformed xml: task block");
            }
            Task t = new Task(title, computationCost);
            tasksMap.put(title, t);
            tasks.add(t);
            computationCost = -1;
            title = null;
        } else if (localName.equalsIgnoreCase(TITLE)) {
            title = text.toString();
        } else if (localName.equalsIgnoreCase(COMPUTATION_COST)) {
            computationCost = Integer.parseInt(text.toString());
        } else if (localName.equalsIgnoreCase(TASKS)) {
            tasks = Collections.unmodifiableList(tasks);
        } else if (localName.equalsIgnoreCase(PRECEDENCES)) {
            precedences = Collections.unmodifiableList(precedences);
        } else if (localName.equalsIgnoreCase(PRECEDENCE)) {
            Task parentTask = tasksMap.get(parent);
            Task childTask = tasksMap.get(child);
            if (parentTask == null || childTask == null) {
                throw new SAXException("malformed xml: precedence block");
            }
            precedences.add(new PrecedenceConstraint(parentTask, childTask, transportationCost));
        } else if (localName.equalsIgnoreCase(PARENT)) {
            parent = text.toString();
            text = new StringBuffer();
        } else if (localName.equalsIgnoreCase(CHILD)) {
            child = text.toString();
            text = new StringBuffer();
        } else if (localName.equalsIgnoreCase(TRANSPORTATION_COST)) {
            transportationCost = Integer.parseInt(text.toString());
        } else if (localName.equalsIgnoreCase(PROBLEM)) {
            problem = new Problem(tasks, precedences, processorsCount);
        }

        super.endElement(uri, localName, qName);
    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
    }

    public Problem getProblem() {
        return problem;
    }
}