package model;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class PrecedenceConstraint {
    private final Task parent;
    private final Task child;
    private final float transportationCost;

    public PrecedenceConstraint(Task parent, Task child, float transportationCost) {
        this.parent = parent;
        this.child = child;
        this.transportationCost = transportationCost;
    }

    public Task getParent() {
        return parent;
    }

    public Task getChild() {
        return child;
    }

    public float getTransportationCost() {
        return transportationCost;
    }
}
