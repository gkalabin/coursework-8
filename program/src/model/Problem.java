package model;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.*;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class Problem {
    private final List<Task> tasks;
    private final List<PrecedenceConstraint> precedences;
    private final int processorsCount;


    public Problem(@NotNull List<Task> tasks, @Nullable List<PrecedenceConstraint> precedences, int processorsCount) {
        this.processorsCount = processorsCount;
        this.tasks = Collections.unmodifiableList(tasks);
        if (precedences == null) {
            this.precedences = Collections.emptyList();
        } else {
            this.precedences = Collections.unmodifiableList(precedences);
        }
    }

    public int getProcessorsCount() {
        return processorsCount;
    }

    public List<PrecedenceConstraint> getPrecedences() {
        return precedences;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public List<PrecedenceConstraint> getAllParentConstraints(Task child) {
        List<PrecedenceConstraint> constraints = new ArrayList<PrecedenceConstraint>();
        for (PrecedenceConstraint pc: precedences) {
            if (pc.getChild().equals(child)) {
                constraints.add(pc);
            }
        }
        return constraints;
    }

    public List<PrecedenceConstraint> getAllChildConstraints(Task parent) {
        List<PrecedenceConstraint> constraints = new ArrayList<PrecedenceConstraint>();
        for (PrecedenceConstraint pc: precedences) {
            if (pc.getParent().equals(parent)) {
                constraints.add(pc);
            }
        }
        return constraints;
    }

    public Set<Task> getAllChildTasks(Task parent) {
        Set<Task> childTasks = new HashSet<Task>();
        for (PrecedenceConstraint pc: precedences) {
            if (pc.getParent().equals(parent)) {
                childTasks.add(pc.getChild());
            }
        }
        return childTasks;
    }

    public List<Task> getAgenda(Collection<Task> processedTasks) {
        List<Task> result = new ArrayList<Task>();
        for (Task t: tasks) {
            if (processedTasks.contains(t)) {
                continue;
            }
            boolean isAvailable = true;
            for (PrecedenceConstraint pc: getAllParentConstraints(t)) {
                isAvailable &= processedTasks.contains(pc.getParent());
            }
            if (isAvailable) {
                result.add(t);
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return "Problem{" +
                "tasks=" + tasks +
                ", precedences=" + precedences +
                ", processorsCount=" + processorsCount +
                '}';
    }
}
