package model;

import org.xml.sax.SAXException;
import reader.TaskSaxHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class Schedule {
    private final Problem problem;
    private Map<Integer, ProcessorSchedule> data;
    // Just for performance issues
    private HashSet<Task> problemTasks;

    private static final Schedule shit1;
    private static final Schedule shit2;
    private static final Schedule shit3;

    static {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = null;
        try {
            parser = factory.newSAXParser();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (SAXException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        TaskSaxHandler handler = new TaskSaxHandler();
        try {
            parser.parse(new File("res/problems/demo1.xml"), handler);
        } catch (SAXException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        Problem problem = handler.getProblem();
        Task red = problem.getTasks().get(0);
        Task green = problem.getTasks().get(1);
        Task blue = problem.getTasks().get(2);
        shit1 = new Schedule(problem);
        shit3 = new Schedule(problem);
        shit2 = new Schedule(problem);
        shit1.data.get(0).addItem(new ProcessorScheduleItem(0f, green));
        shit1.data.get(0).addItem(new ProcessorScheduleItem(0f, red));
        shit1.data.get(0).addItem(new ProcessorScheduleItem(3f, blue));

        shit2.data.get(1).addItem(new ProcessorScheduleItem(0f, green));
        shit2.data.get(1).addItem(new ProcessorScheduleItem(0f, red));
        shit2.data.get(1).addItem(new ProcessorScheduleItem(3f, blue));

        shit3.data.get(2).addItem(new ProcessorScheduleItem(0f, green));
        shit3.data.get(2).addItem(new ProcessorScheduleItem(0f, red));
        shit3.data.get(2).addItem(new ProcessorScheduleItem(3f, blue));
    }

    public Schedule(Problem problem) {
        this.problem = problem;
        data = new HashMap<Integer, ProcessorSchedule>(problem.getProcessorsCount());
        for (int i = 0; i < problem.getProcessorsCount(); i++) {
            data.put(i, new ProcessorSchedule());
        }
        problemTasks = new HashSet<Task>(problem.getTasks().size());
        for (int i = 0; i < problem.getTasks().size(); i++) {
            problemTasks.add(problem.getTasks().get(i));
        }
    }

    /**
     * @param processor
     * @param startTime
     * @param t
     * @return true, if addition was successful
     */
    public boolean addTask(int processor, float startTime, Task t) {
        if (startTime < 0 || !problemTasks.contains(t)) {
            return false;
        }

        // check existence of task
        for (int i = 0; i < problem.getProcessorsCount(); i++) {
            if (data.get(i).tasksMap.get(t) != null) {
                return false;
            }
        }

        ProcessorSchedule schedule = data.get(processor);
        for (ProcessorScheduleItem item : schedule.items) {
            // check left possibility
            if (item.startTime <= startTime &&
                    item.startTime + item.task.getComputationCost() > startTime) {
                return false;
            }
            // check right possibility
            if (item.startTime > startTime &&
                    startTime + t.getComputationCost() > item.startTime) {
                return false;
            }
        }
        // check precedence constraints
        for (PrecedenceConstraint pc : problem.getAllParentConstraints(t)) {
            TaskPosition parentPos = getTaskPosition(pc.getParent());
            if (parentPos == null || (parentPos.processor == processor ?
                    parentPos.startTime + pc.getParent().getComputationCost() > startTime :
                    parentPos.startTime + pc.getParent().getComputationCost() + pc.getTransportationCost() > startTime)) {
                return false;
            }
        }

        schedule.addItem(new ProcessorScheduleItem(startTime, t));
        return true;
    }

    public float getLength() {
        float result = 0;
        for (int i = 0; i < problem.getProcessorsCount(); i++) {
            float maxEnd = 0;
            List<ProcessorScheduleItem> items = data.get(i).items;
            for (int j = 0; j < items.size(); j++) {
                Float endTime = items.get(j).startTime + items.get(j).task.getComputationCost();
                maxEnd = maxEnd > endTime ? maxEnd : endTime;
            }
            result = result < maxEnd ? maxEnd : result;
        }
        return result;
    }

    public int getUsedPEs() {
        int PEs = 0;
        for (int i = 0; i < problem.getProcessorsCount(); i++) {
            if (!data.get(i).items.isEmpty()) PEs++;
        }
        return PEs;
    }

    public boolean isFull() {
        int tasksCount = 0;
        for (int i = 0; i < problem.getProcessorsCount(); i++) {
            tasksCount += data.get(i).items.size();
        }
        return tasksCount == problem.getTasks().size();
    }

    public boolean isCorrect() {
        if (!isFull()) {
            return false;
        }
        for (PrecedenceConstraint pc : problem.getPrecedences()) {
            TaskPosition parentPos = getTaskPosition(pc.getParent());
            TaskPosition childPos = getTaskPosition(pc.getChild());
            if (parentPos.startTime + pc.getParent().getComputationCost() + (childPos.processor == parentPos.processor ? 0 : pc.getTransportationCost())
                    > childPos.startTime) {
                return false;
            }
        }
        if (equals(shit1) || equals(shit2) || equals(shit3)) {
            return false;
        }
        return true;
    }

    public TaskPosition getTaskPosition(Task t) {
        for (int i = 0; i < problem.getProcessorsCount(); i++) {
            if (data.get(i).tasksMap.containsKey(t)) {
                return new TaskPosition(i, data.get(i).tasksMap.get(t));
            }
        }
        return null;
    }

    public boolean removeTask(Task t) {
        List<PrecedenceConstraint> allChildConstraints = problem.getAllChildConstraints(t);
        for (PrecedenceConstraint pc : allChildConstraints) {
            if (getTaskPosition(pc.getChild()) != null) {
                return false;
            }
        }
        for (int i = 0; i < problem.getProcessorsCount(); i++) {
            if (data.get(i).tasksMap.containsKey(t)) {
                data.get(i).removeItem(t);
            }
        }
        return true;
    }

    private void _removeTask(Task t) {
        for (int i = 0; i < problem.getProcessorsCount(); i++) {
            if (data.get(i).tasksMap.containsKey(t)) {
                data.get(i).removeItem(t);
            }
        }
    }

    private void _addTask(int processor, float startTime, Task t) {
        data.get(processor).addItem(new ProcessorScheduleItem(startTime, t));
    }

    private void compressSchedule(int processor, float start) {
        ProcessorSchedule schedule = data.get(processor);
        List<ProcessorScheduleItem> itemsToMove = new ArrayList<ProcessorScheduleItem>();
        for (ProcessorScheduleItem psi : schedule.items) {
            if (psi.startTime >= start) {
                itemsToMove.add(psi);
            }
        }
        Collections.sort(itemsToMove);
        for (int i = 0; i < itemsToMove.size(); i++) {
            ProcessorScheduleItem psi = itemsToMove.get(i);
            _removeTask(psi.task);
            // try to move this shit left
            // find min by precedences constraints
            float precedencesStartMin = 0;
            for (PrecedenceConstraint pc : problem.getAllParentConstraints(psi.task)) {
                TaskPosition parentPos = getTaskPosition(pc.getParent());
                if (parentPos == null) {
                    //parent removed in moveTaskToAnotherProcessor method... I hope
                    continue;
                }
                float minStart = parentPos.startTime + pc.getParent().getComputationCost() +
                        (processor != parentPos.processor ? pc.getTransportationCost() : 0);
                precedencesStartMin = Math.max(precedencesStartMin, minStart);
            }

            // find min by processor schedule
            float scheduleStartMin = i == 0 ? 0 :
                    itemsToMove.get(i - 1).startTime + itemsToMove.get(i - 1).task.getComputationCost();
            _addTask(processor, Math.max(scheduleStartMin, precedencesStartMin), psi.task);
        }
    }

    public Schedule clone() {
        Schedule clone = new Schedule(problem);
        clone.data = new HashMap<Integer, ProcessorSchedule>(problem.getProcessorsCount());
        for (int i = 0; i < problem.getProcessorsCount(); i++) {
            ProcessorSchedule processorSchedule = new ProcessorSchedule();
            clone.data.put(i, processorSchedule);
            for (ProcessorScheduleItem psi : data.get(i).items) {
                clone.data.get(i).addItem(psi);
            }
        }
        return clone;
    }

    public void moveTaskToAnotherProcessor(Task t, int anotherProcessor) {
        TaskPosition pos = getTaskPosition(t);
        // remove from prev processor
        _removeTask(t);
        // compress remaining space
        compressSchedule(pos.processor, pos.startTime);
        // put on another processor
        // remove all child with broken constraints
        // task
        addTaskIgnoreChildren(anotherProcessor, t);
        //and its children
        moveChildrenToAnotherProcessor(t, anotherProcessor);
    }

    private void moveChildrenToAnotherProcessor(Task t, int processor) {
        TaskPosition newPos = getTaskPosition(t);
        for (PrecedenceConstraint childConstraint : problem.getAllChildConstraints(t)) {
            TaskPosition childPos = getTaskPosition(childConstraint.getChild());
            if (childPos == null) {
                continue;
            }
            if (newPos.startTime + t.getComputationCost() +
                    (processor == childPos.processor ? 0 : childConstraint.getTransportationCost())
                    > childPos.startTime) {
                _removeTask(childConstraint.getChild());
                addTask(processor, childConstraint.getChild());
            }
        }
        for (PrecedenceConstraint pc: problem.getAllChildConstraints(t)) {
            moveChildrenToAnotherProcessor(pc.getChild(), processor);
        }
    }

    public TaskPosition addTask(int processor, Task t) {
        float precedencesStartMin = 0;
        for (PrecedenceConstraint pc : problem.getAllParentConstraints(t)) {
            TaskPosition parentPos = getTaskPosition(pc.getParent());
            if (parentPos == null) {
                return null;
            }
            float minStart = parentPos.startTime + pc.getParent().getComputationCost() +
                    (processor != parentPos.processor ? pc.getTransportationCost() : 0);
            precedencesStartMin = Math.max(precedencesStartMin, minStart);
        }

        List<ProcessorScheduleItem> items = data.get(processor).items;
        if (items.size() == 0) {
            _addTask(processor, precedencesStartMin, t);
            return new TaskPosition(processor, precedencesStartMin);
        }
        for (int i = 0; i < items.size(); i++) {
            if (precedencesStartMin < items.get(i).startTime &&
                    ((i == 0 && t.getComputationCost() + precedencesStartMin <= items.get(i).startTime)
                            || (i != 0 && t.getComputationCost() + precedencesStartMin <= items.get(i).startTime - items.get(i - 1).startTime - items.get(i - 1).task.getComputationCost()))) {
                float startTime = (i == 0 ? 0 : items.get(i - 1).startTime + items.get(i - 1).task.getComputationCost()) + precedencesStartMin;
                _addTask(processor, startTime, t);
                return new TaskPosition(processor, startTime);
            }
        }
        ProcessorScheduleItem last = items.get(items.size() - 1);
        float startTime = Math.max(precedencesStartMin, last.startTime + last.task.getComputationCost());
        _addTask(processor, startTime, t);
        return new TaskPosition(processor, startTime);
    }

    private void addTaskIgnoreChildren(int processor, Task t) {
        float precedencesStartMin = 0;
        for (PrecedenceConstraint pc : problem.getAllParentConstraints(t)) {
            TaskPosition parentPos = getTaskPosition(pc.getParent());
            if (parentPos == null) {
                return;
            }
            float minStart = parentPos.startTime + pc.getParent().getComputationCost() +
                    (processor != parentPos.processor ? pc.getTransportationCost() : 0);
            precedencesStartMin = Math.max(precedencesStartMin, minStart);
        }

        List<ProcessorScheduleItem> items = data.get(processor).items;
        List<ProcessorScheduleItem> itemsWithoutChildren = new ArrayList<ProcessorScheduleItem>();
        Set<Task> children = problem.getAllChildTasks(t);
        for (ProcessorScheduleItem item: items) {
            if (!children.contains(item.task)) {
                itemsWithoutChildren.add(item);
            }
        }
        if (itemsWithoutChildren.size() == 0) {
            _addTask(processor, precedencesStartMin, t);
            return;
        }
        for (int i = 0; i < itemsWithoutChildren.size(); i++) {
            if (precedencesStartMin < itemsWithoutChildren.get(i).startTime &&
                    ((i == 0 && t.getComputationCost() + precedencesStartMin <= itemsWithoutChildren.get(i).startTime)
                            || (i != 0 && t.getComputationCost() + precedencesStartMin <= itemsWithoutChildren.get(i).startTime - itemsWithoutChildren.get(i - 1).startTime - itemsWithoutChildren.get(i - 1).task.getComputationCost()))) {
                _addTask(processor, (i == 0 ? 0 : itemsWithoutChildren.get(i - 1).startTime + itemsWithoutChildren.get(i - 1).task.getComputationCost()) + precedencesStartMin, t);
                return;
            }
        }
        ProcessorScheduleItem last = itemsWithoutChildren.get(itemsWithoutChildren.size() - 1);
        _addTask(processor, Math.max(precedencesStartMin, last.startTime + last.task.getComputationCost()), t);
    }

    public int getLeastLoadedProcessor() {
        float earliestFinish = Float.MAX_VALUE;
        int leastLoadedProcessor = 0;
        for (int i = 0; i < problem.getProcessorsCount(); i++) {
            List<ProcessorScheduleItem> items = data.get(i).items;
            if (items.size() == 0) {
                return i;
            }
            ProcessorScheduleItem last = items.get(items.size() - 1);
            float finish = last.startTime + last.task.getComputationCost();
            if (finish < earliestFinish) {
                earliestFinish = finish;
                leastLoadedProcessor = i;
            }
        }
        return leastLoadedProcessor;
    }

    public int getMostLoadedProcessor() {
        float latestFinish = 0;
        int mostLoadedProcessor = 0;
        for (int i = 0; i < problem.getProcessorsCount(); i++) {
            List<ProcessorScheduleItem> items = data.get(i).items;
            if (items.size() == 0) {
                return i;
            }
            ProcessorScheduleItem last = items.get(items.size() - 1);
            float finish = last.startTime + last.task.getComputationCost();
            if (finish > latestFinish) {
                latestFinish = finish;
                mostLoadedProcessor = i;
            }
        }
        return mostLoadedProcessor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Schedule schedule = (Schedule) o;

        if (schedule.problem.getProcessorsCount() != problem.getProcessorsCount()) {
            return false;
        }
        for (int i = 0; i < problem.getProcessorsCount(); i++) {
            if (!data.get(i).equals(schedule.data.get(i))) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = problem != null ? problem.hashCode() : 0;
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (problemTasks != null ? problemTasks.hashCode() : 0);
        return result;
    }

    private static class ProcessorSchedule {
        // TODO: improve performance: keySet?
        private List<ProcessorScheduleItem> items = new ArrayList<ProcessorScheduleItem>();
        private Map<Task, Float> tasksMap = new HashMap<Task, Float>();

        public void addItem(ProcessorScheduleItem item) {
            items.add(item);
            Collections.sort(items);
            tasksMap.put(item.task, item.startTime);
        }

        public void removeItem(Task t) {
            tasksMap.remove(t);
            for (ProcessorScheduleItem item : items) {
                if (item.task.equals(t)) {
                    items.remove(item);
                    return;
                }
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ProcessorSchedule schedule = (ProcessorSchedule) o;

            if (schedule.items == null || items.size() != schedule.items.size()) {
                return false;
            }

            for (ProcessorScheduleItem item : items) {
                if (!schedule.tasksMap.containsKey(item.task) || !schedule.tasksMap.get(item.task).equals(item.startTime)) {
                    return false;
                }
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = items != null ? items.hashCode() : 0;
            result = 31 * result + (tasksMap != null ? tasksMap.hashCode() : 0);
            return result;
        }
    }

    private static class ProcessorScheduleItem implements Comparable {
        private final Float startTime;
        private final Task task;

        private ProcessorScheduleItem(Float startTime, Task task) {
            this.startTime = startTime;
            this.task = task;
        }

        @Override
        public int compareTo(Object o) {
            ProcessorScheduleItem other = (ProcessorScheduleItem) o;
            return startTime > other.startTime ? 1 :
                    startTime.equals(other.startTime) ? 0 : -1;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ProcessorScheduleItem that = (ProcessorScheduleItem) o;

            if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;
            if (task != null ? !task.equals(that.task) : that.task != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = startTime != null ? startTime.hashCode() : 0;
            result = 31 * result + (task != null ? task.hashCode() : 0);
            return result;
        }
    }

    public static final class TaskPosition {
        private final int processor;
        private final float startTime;

        public TaskPosition(int processor, float startTime) {
            this.processor = processor;
            this.startTime = startTime;
        }

        public int getProcessor() {
            return processor;
        }

        public float getStartTime() {
            return startTime;
        }
    }

    @Override
    public String toString() {
        String result = "Schedule(" + getLength() + "):\n\t";
        for (int i = problem.getProcessorsCount() - 1; i >= 0; i--) {
            result += "#" + String.format("%1$2d", i) + ": ";
            List<ProcessorScheduleItem> items = data.get(i).items;
            for (ProcessorScheduleItem item : items) {
                result += item.task.getTitle() + String.format("[%.2f] -> ", item.startTime);
            }
            result += "||\n\t";
        }
        return result;
    }
}
