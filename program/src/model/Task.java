package model;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class Task {
    private final float computationCost;
    private final String title;

    public Task(String title, float computationCost) {
        this.computationCost = computationCost;
        this.title = title;
    }

    public float getComputationCost() {
        return computationCost;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (Float.compare(task.computationCost, computationCost) != 0) return false;
        if (title != null ? !title.equals(task.title) : task.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (computationCost != +0.0f ? Float.floatToIntBits(computationCost) : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return title+String.format("(%.2f)",computationCost);
    }
}
