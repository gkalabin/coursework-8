package runnable;

import algorithm.GeneticAlgorithm;
import algorithm.MS2DCAlgorithm;
import model.Problem;
import org.xml.sax.SAXException;
import reader.TaskSaxHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class MS2DC {
    public static final File FILE1 = new File("res/problems/demo1.xml");
    public static final File FILE2 = new File("res/problems/demo2.xml");
    public static final File FILE3 = new File("res/problems/alphabet.xml");

    public static void main(String[] args) throws SAXException, ParserConfigurationException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        TaskSaxHandler handler = new TaskSaxHandler();
        parser.parse(FILE3, handler);
        Problem problem = handler.getProblem();
        MS2DCAlgorithm g = new MS2DCAlgorithm(problem);
        System.out.println(g.solve());

    }
}
