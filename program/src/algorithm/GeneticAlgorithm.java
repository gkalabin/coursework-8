package algorithm;

import model.PrecedenceConstraint;
import model.Problem;
import model.Schedule;
import model.Task;

import java.util.*;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class GeneticAlgorithm {
    private static final int ITERATIONS_COUNT = 100;
    private static final int POPULATION_SIZE = 500;
    // probabilities
    private static final double MUTATION_PROBABILITY = .1d;
    private static final double CROSSOVER_PROBABILITY = .85d;

    private final Problem problem;
    private List<Schedule> population;

    public GeneticAlgorithm(Problem problem) {
        this.problem = problem;
    }

    public Schedule solve() {
        generatePopulation();
        for (int i = 0; i < ITERATIONS_COUNT; i++) {
            mutate();
            crossover();
            select();
        }
        return population.get(0);
    }

    private void mutate() {
        for (int i = 0; i < POPULATION_SIZE; i++) {
            if (Math.random() < MUTATION_PROBABILITY) {
                // TEENAGE MUTANT NINJA TURTLES
                Task task = problem.getTasks().get((int) (Math.random() * problem.getTasks().size()));
                Schedule s = population.get(i).clone();
                s.moveTaskToAnotherProcessor(task, (int) (Math.random() * problem.getProcessorsCount()));
                if (s.isCorrect()) {
                    population.add(s);
                }
            }
        }
    }

    private void crossover() {
        List<Schedule> populationCopy = new ArrayList<Schedule>(population);
        int crossoverCount = (int) (problem.getTasks().size() * CROSSOVER_PROBABILITY / 2);
        for (int i = 0; i < crossoverCount; i++) {
            int motherIdx = (int) (Math.random() * populationCopy.size());
            Schedule mother = populationCopy.get(motherIdx);
            populationCopy.remove(motherIdx);
            int fatherIdx = (int) (Math.random() * populationCopy.size());
            Schedule father = populationCopy.get(fatherIdx);
            populationCopy.remove(fatherIdx);
            Schedule son = new Schedule(problem);
            Schedule daughter = new Schedule(problem);

            Set<Task> processedTasks = new HashSet<Task>(problem.getTasks().size());
            while (problem.getTasks().size() != processedTasks.size()) {
                List<Task> agenda = problem.getAgenda(processedTasks);
                if (agenda.isEmpty()) {
                    throw new RuntimeException("wtf? agenda? what a u doing?");
                }

                for (Task t : agenda) {
                    Schedule.TaskPosition fatherPos = father.getTaskPosition(t);
                    Schedule.TaskPosition motherPos = mother.getTaskPosition(t);
                    son.addTask(fatherPos.getProcessor(), t);
                    daughter.addTask(motherPos.getProcessor(), t);
                    processedTasks.add(t);
                }
            }

            if (son.isCorrect() && daughter.isCorrect()) {
                population.add(son);
                population.add(daughter);
            }

        }
    }

    private void select() {
        final Map<Schedule, Float> estimates = new HashMap<Schedule, Float>(population.size());
        for (int i = 0; i < population.size(); i++) {
            Schedule schedule = population.get(i);
            float est = schedule.getLength() * 100 + schedule.getUsedPEs() * 8;
            estimates.put(schedule, est);
        }

        Collections.sort(population, new Comparator<Schedule>() {
            @Override
            public int compare(Schedule o1, Schedule o2) {
                Float o2est = estimates.get(o2);
                Float o1est = estimates.get(o1);
                return o1est > o2est ? 1 :
                        o1est == o2est ? 0 : -1;
            }
        });

        population = population.subList(0, POPULATION_SIZE);
    }

    private void generatePopulation() {
        population = new ArrayList<Schedule>(POPULATION_SIZE);
        while (population.size() <= POPULATION_SIZE) {
            double rnd = Math.random();
            Schedule newSchedule;
            if (rnd < .2d) {
                newSchedule = generateGreedySchedule();
            } else if (rnd < 4.d) {
                newSchedule = generateLineOnOneProcessorSchedule();
            } else {
                newSchedule = generateRandomSchedule();
            }
            if (newSchedule != null && newSchedule.isCorrect()) {
                population.add(newSchedule);
            }
        }
    }

    private Schedule generateLineOnOneProcessorSchedule() {
        Schedule s = new Schedule(problem);
        Set<Task> remainedTasks = new HashSet<Task>(problem.getTasks());
        int processor = (int) (Math.random() * problem.getProcessorsCount());
        float startTime = 0;
        for (PrecedenceConstraint pc : problem.getPrecedences()) {
            Task task = pc.getParent();
            if (remainedTasks.contains(task)) {
                if (!s.addTask(processor, startTime, task)) {
                    return null;
                }
                startTime += task.getComputationCost();
                remainedTasks.remove(task);
            }
        }
        for (Task task : problem.getTasks()) {
            if (remainedTasks.contains(task)) {
                if (!s.addTask(processor, startTime, task)) {
                    return null;
                }
                startTime += task.getComputationCost();
                remainedTasks.remove(task);
            }
        }
        return s;
    }

    private Schedule generateGreedySchedule() {
        Schedule s = new Schedule(problem);
        Set<Task> processed = new HashSet<Task>(problem.getTasks().size());
        while (processed.size() != problem.getTasks().size()) {
            List<Task> agenda = problem.getAgenda(processed);
            for (Task t : agenda) {
                s.addTask(s.getLeastLoadedProcessor(), t);
                processed.add(t);
            }
        }
        return s;
    }

    private Schedule generateRandomSchedule() {
        Schedule s = new Schedule(problem);
        Set<Task> processed = new HashSet<Task>(problem.getTasks().size());
        while (processed.size() != problem.getTasks().size()) {
            List<Task> agenda = problem.getAgenda(processed);
            for (Task t : agenda) {
                s.addTask((int) (Math.random() * problem.getProcessorsCount()), t);
                processed.add(t);
            }
        }
        return s;
    }
}
