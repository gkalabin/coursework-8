package algorithm;

import model.PrecedenceConstraint;
import model.Problem;
import model.Schedule;
import model.Task;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class MS2DCAlgorithm {
    private final Problem problem;
    private Schedule schedule;

    public MS2DCAlgorithm(Problem problem) {
        this.problem = problem;
    }

    public Schedule solve() {
        schedule = new Schedule(problem);
        Set<Task> processedTasks = new HashSet<Task>(problem.getTasks().size());
        while (processedTasks.size() < problem.getTasks().size()) {
            // find most constrained task from agenda
            Task mostConstrainedTask = getMostConstrainedTask(processedTasks);
            // find least impact solution
            int[] parentsOnProcessor = new int[problem.getProcessorsCount()];
            for (PrecedenceConstraint pc: problem.getAllParentConstraints(mostConstrainedTask)) {
                parentsOnProcessor[schedule.getTaskPosition(pc.getParent()).getProcessor()]++;
            }
            int processor = 0;
            int maxParents = 0;
            for (int i = 0; i <problem.getProcessorsCount(); i++) {
                if (parentsOnProcessor[i] > maxParents) {
                    processor = i;
                    maxParents = parentsOnProcessor[i];
                }
            }
            schedule.addTask((maxParents == 0 ? schedule.getMostLoadedProcessor() : processor), mostConstrainedTask);
            processedTasks.add(mostConstrainedTask);
        }

        return schedule;
    }

    private Task getMostConstrainedTask(Set<Task> processedTasks) {
        List<Task> agenda = problem.getAgenda(processedTasks);
        if (agenda.isEmpty()) {
            throw new RuntimeException("invalid problem: cyclic dependencies?");
        }
        Task mostConstrainedTask = agenda.get(0);
        int maxConstrainsCount = problem.getAllChildConstraints(mostConstrainedTask).size();
        for (Task t : agenda) {
            int constraintsCount = problem.getAllChildConstraints(t).size();
            if (constraintsCount > maxConstrainsCount) {
                maxConstrainsCount = constraintsCount;
                mostConstrainedTask = t;
            }
        }
        return mostConstrainedTask;
    }
}
