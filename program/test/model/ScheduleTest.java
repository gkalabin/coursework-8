package model;

import junit.framework.TestCase;
import reader.TaskSaxHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ScheduleTest extends TestCase {
    private Problem problem;
    private Task red;
    private Task green;
    private Task blue;

    public void setUp() throws Exception {
        super.setUp();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        TaskSaxHandler handler = new TaskSaxHandler();
        parser.parse(new File("res/problems/demo1.xml"), handler);
        problem = handler.getProblem();
        red = problem.getTasks().get(0);
        green = problem.getTasks().get(1);
        blue = problem.getTasks().get(2);
    }

    public void testAddition() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(0, 0, red));
        assertEquals(3f, s.getLength());
        assertEquals(1, s.getUsedPEs());

        Schedule s2 = new Schedule(problem);
        assertFalse(s2.addTask(0, 0, blue));
        assertEquals(0f, s2.getLength());
        assertEquals(0, s2.getUsedPEs());
    }

    public void testEmpty() throws Exception {
        Schedule s = new Schedule(problem);
        assertEquals(0f, s.getLength());
        assertEquals(0, s.getUsedPEs());
    }

    public void testClone() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(0, 0, red));
        assertTrue(s.addTask(1, 0, green));
        assertTrue(s.addTask(0, 3, blue));
        Schedule clone = s.clone();
        assertTrue(clone.removeTask(blue));
        assertEquals(10f, s.getLength());
        assertTrue(s.isFull());
        assertEquals(2, s.getUsedPEs());
        assertEquals(5f, clone.getLength());
        assertFalse(clone.isFull());
        assertEquals(2, clone.getUsedPEs());
    }

    public void testDuplicate() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(0, 0, red));
        assertFalse(s.addTask(1, 0, red));
        assertEquals(3f, s.getLength());
        assertEquals(1, s.getUsedPEs());
    }

    public void testLeftIntersection() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(0, 1, red));
        assertFalse(s.addTask(0, 2, blue));
        assertEquals(4f, s.getLength());
        assertEquals(1, s.getUsedPEs());
    }

    public void testRightIntersection() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(0, 1, red));
        assertFalse(s.addTask(0, 0.5f, blue));
        assertEquals(4f, s.getLength());
        assertEquals(1, s.getUsedPEs());
    }

    public void testFull() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(0, 0, red));
        assertTrue(s.addTask(1, 0, green));
        assertTrue(s.addTask(0, 3, blue));
        assertEquals(10f, s.getLength());
        assertTrue(s.isFull());
        assertEquals(2, s.getUsedPEs());
    }

    public void testFullCrossprocessor() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(0, 0, red));
        assertTrue(s.addTask(1, 0, green));
        assertTrue(s.addTask(2, 4, blue));
        assertEquals(11f, s.getLength());
        assertTrue(s.isFull());
        assertEquals(3, s.getUsedPEs());
    }

    public void testAddition2() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(0, 5f, red));
        s.addTask(0, green);
        assertEquals(8f, s.getLength());
        assertEquals(1, s.getUsedPEs());
        assertTrue(s.removeTask(green));
        assertTrue(s.removeTask(red));

        assertTrue(s.addTask(0, 8f, red));
        s.addTask(0, blue);
        assertEquals(18f, s.getLength());
        assertEquals(1, s.getUsedPEs());
        assertFalse(s.removeTask(red));
        assertTrue(s.removeTask(blue));
        assertTrue(s.removeTask(red));

        assertTrue(s.addTask(0, 3f, red));
        s.addTask(0, green);
        assertEquals(11f, s.getLength());
        assertEquals(1, s.getUsedPEs());
    }

    public void testMove() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(0, 0, red));
        assertTrue(s.addTask(1, 0, green));
        assertTrue(s.addTask(2, 4, blue));
        s.moveTaskToAnotherProcessor(blue, 0);
        assertEquals(10f, s.getLength());
        assertTrue(s.isFull());
        assertEquals(2, s.getUsedPEs());
    }

    public void testMoveHere() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(0, 0, red));
        assertTrue(s.addTask(1, 0, green));
        assertTrue(s.addTask(2, 4, blue));
        s.moveTaskToAnotherProcessor(red,0);
        assertEquals(11f, s.getLength());
        assertTrue(s.isFull());
        assertEquals(3, s.getUsedPEs());
        s.moveTaskToAnotherProcessor(green,1);
        assertEquals(11f, s.getLength());
        assertTrue(s.isFull());
        assertEquals(3, s.getUsedPEs());
        s.moveTaskToAnotherProcessor(blue,2);
        assertEquals(11f, s.getLength());
        assertTrue(s.isFull());
        assertEquals(3, s.getUsedPEs());
    }

    public void testMoveOutFromProcessor() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(0, 0, red));
        assertTrue(s.addTask(1, 0, green));
        assertTrue(s.addTask(0, 3, blue));
        s.moveTaskToAnotherProcessor(blue,2);
        assertEquals(11f, s.getLength());
        assertTrue(s.isFull());
        assertEquals(3, s.getUsedPEs());
    }

    public void testMoveParent() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(0, 0, red));
        assertTrue(s.addTask(1, 0, green));
        assertTrue(s.addTask(2, 4, blue));
        s.moveTaskToAnotherProcessor(red,1);
        assertEquals(15f, s.getLength());
        assertTrue(s.isFull());
        assertEquals(1, s.getUsedPEs());
        assertTrue(s.isCorrect());
    }

    public void testConsistencyBreak() throws Exception {
        // green[0] -> red [0] -> blue[3] situation
        Schedule s = new Schedule(problem);
//        assertTrue(s.addTask(1, 0, green));
//        assertFalse(s.addTask(1, 0, red));
//
//        s = new Schedule(problem);
//        assertTrue(s.addTask(2, 0, green));
//        assertTrue(s.addTask(1, 0, red));
//        assertTrue(s.addTask(0, 4, blue));
//        s.moveTaskToAnotherProcessor(red,2);
//        assertTrue(s.isFull());
//        assertTrue(s.isCorrect());
//        assertEquals(15f, s.getLength());
//
//        s = new Schedule(problem);
//        assertTrue(s.addTask(2, 0, green));
//        assertTrue(s.addTask(1, 0, red));
//        assertTrue(s.addTask(1, 3, blue));
//        s.moveTaskToAnotherProcessor(red,2);
//        assertTrue(s.isFull());
//        assertTrue(s.isCorrect());
//        assertEquals(15f, s.getLength());
//
//        s = new Schedule(problem);
//        assertTrue(s.addTask(2, 0, green));
//        assertTrue(s.addTask(1, 0, red));
//        assertTrue(s.addTask(0, 4, blue));
//        s.moveTaskToAnotherProcessor(green,1);
//        assertTrue(s.isFull());
//        assertTrue(s.isCorrect());
//        assertEquals(11f, s.getLength());
//
//        s = new Schedule(problem);
//        assertTrue(s.addTask(2, 0, green));
//        assertTrue(s.addTask(1, 0, red));
//        assertTrue(s.addTask(1, 3, blue));
//        s.moveTaskToAnotherProcessor(green,1);
//        assertTrue(s.isFull());
//        assertTrue(s.isCorrect());
//        assertEquals(15f, s.getLength());

        s = new Schedule(problem);
        assertTrue(s.addTask(2, 0, red));
        assertTrue(s.addTask(1, 0, green));
        assertTrue(s.addTask(1, 5, blue));
        s.moveTaskToAnotherProcessor(red,1);
        assertTrue(s.isFull());
        assertTrue(s.isCorrect());
        assertEquals(15f, s.getLength());
    }

    public void testEquals() throws Exception {
        Schedule s = new Schedule(problem);
        assertTrue(s.addTask(2, 0, green));
        assertTrue(s.addTask(1, 0, red));
        assertTrue(s.addTask(1, 3, blue));
        Schedule s2 = new Schedule(problem);
        assertTrue(s2.addTask(2, 0, green));
        assertTrue(s2.addTask(1, 0, red));
        assertTrue(s2.addTask(1, 3, blue));
        assertTrue(s.equals(s2));
    }

    public void testCompress() throws Exception {
//        Schedule s = new Schedule(problem);
//        Task red = problem.getTasks().get(0);
//        Task green = problem.getTasks().get(1);
//        Task blue = problem.getTasks().get(2);
//        s.addTask(0, 0, red);
//        s.addTask(0, 3f, green);
//        s.addTask(0, 8f, blue);
//
//        // remove 'blue'
//        s.removeTask(blue);
//        s.compressSchedule(0, 0f);
//        assertEquals(8f, s.getLength());
//        assertFalse(s.isFull());
//        assertEquals(1, s.getUsedPEs());
//        s.addTask(0, 8f, blue);
//        // remove 'green'
//        s.removeTask(green);
//        s.compressSchedule(0, 0f);
//        assertEquals(10f, s.getLength());
//        assertFalse(s.isFull());
//        assertEquals(1, s.getUsedPEs());
//        s.removeTask(red);
//        s.removeTask(blue);
//        s.addTask(0, 0, red);
//        s.addTask(0, 3f, green);
//        s.addTask(0, 8f, blue);
//        // remove 'red'
//        s.removeTask(red);
//        s.compressSchedule(0, 0f);
//        assertEquals(12f, s.getLength());
//        assertFalse(s.isFull());
//        assertEquals(1, s.getUsedPEs());
    }
}
