package model;

import junit.framework.TestCase;
import reader.TaskSaxHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ProblemTest extends TestCase {
    private Problem problem;
    private Task red;
    private Task green;
    private Task blue;

    public void setUp() throws Exception {
        super.setUp();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        TaskSaxHandler handler = new TaskSaxHandler();
        parser.parse(new File("res/problems/demo1.xml"), handler);
        problem = handler.getProblem();
        red = problem.getTasks().get(0);
        green = problem.getTasks().get(1);
        blue = problem.getTasks().get(2);
    }

    public void testAgendaEntry() throws Exception {
        List<Task> agenda = problem.getAgenda(Collections.<Task>emptySet());
        assertEquals(2, agenda.size());
        assertTrue(agenda.contains(red));
        assertTrue(agenda.contains(green));
    }

    public void testAgendaExit() throws Exception {
        Set<Task> processed = new HashSet<Task>();
        processed.add(red);
        List<Task> agenda = problem.getAgenda(processed);
        assertEquals(2, agenda.size());
        assertTrue(agenda.contains(blue));
        assertTrue(agenda.contains(green));
    }

    public void testAgendaMisc() throws Exception {
        Set<Task> processed = new HashSet<Task>();
        processed.add(green);
        List<Task> agenda = problem.getAgenda(processed);
        assertEquals(1, agenda.size());
        assertTrue(agenda.contains(red));

        processed = new HashSet<Task>();
        processed.add(green); processed.add(red); processed.add(blue);
        agenda = problem.getAgenda(processed);
        assertEquals(0, agenda.size());
    }
}
