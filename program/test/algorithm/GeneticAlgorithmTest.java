package algorithm;

import junit.framework.TestCase;
import model.Problem;
import model.Schedule;
import reader.TaskSaxHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.lang.reflect.Method;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class GeneticAlgorithmTest extends TestCase {
    private Problem problem;
    @Override
    public void setUp() throws Exception {
        super.setUp();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        TaskSaxHandler handler = new TaskSaxHandler();
        parser.parse(new File("res/problems/demo1.xml"), handler);
        problem = handler.getProblem();
    }

    public void testGenerationOnOneProcessor() throws Exception {
        GeneticAlgorithm g = new GeneticAlgorithm(problem);
        Method method = GeneticAlgorithm.class.getDeclaredMethod("generateLineOnOneProcessorSchedule");
        method.setAccessible(true);
        Schedule s = (Schedule) method.invoke(g);
        assertEquals(15f, s.getLength());
        assertEquals(1, s.getUsedPEs());
        assertTrue(s.isFull());
        assertTrue(s.isCorrect());
    }

    public void testGenerationGreedy() throws Exception {
        GeneticAlgorithm g = new GeneticAlgorithm(problem);
        Method method = GeneticAlgorithm.class.getDeclaredMethod("generateGreedySchedule");
        method.setAccessible(true);
        Schedule s = (Schedule) method.invoke(g);
        assertEquals(11f, s.getLength());
        assertEquals(3, s.getUsedPEs());
        assertTrue(s.isFull());
        assertTrue(s.isCorrect());
    }

    public void testGenerationRandom() throws Exception {
        GeneticAlgorithm g = new GeneticAlgorithm(problem);
        Method method = GeneticAlgorithm.class.getDeclaredMethod("generateRandomSchedule");
        method.setAccessible(true);
        for (int i=0; i<10000; i++) {
            Schedule s = (Schedule) method.invoke(g);
            assertTrue(s.isFull());
            assertTrue(s.isCorrect());
        }
    }
}
